FROM alpine:3.7

# Setup build environment and key dependencies
RUN apk update && apk --no-cache add \
    attr-dev \
    autoconf \
    automake \
    bison \
    boost-dev \
    build-base \
    bzr \
    cmake \
    docbook-xml \
    docbook-xsl \
    flex \
    giflib-dev \
    git \
    gperf \
    libjpeg-turbo-dev \
    libqrencode-dev \
    libxml2-dev \
    libxml2-utils \
    libxrender-dev \
    libxslt-dev \
    lmdb-dev \
    networkmanager-dev \
    perl \
    perl-io-socket-ssl \
    perl-json \
    perl-yaml-libyaml \
    perl-xml-parser \
    polkit-dev \
    qt5-qtbase-dev \
    qt5-qtdeclarative-dev \
    qt5-qtmultimedia-dev \
    qt5-qtscript-dev \
    qt5-qtsvg-dev \
    qt5-qttools-dev \
    qt5-qtwebkit-dev \
    qt5-qtxmlpatterns-dev \
    texinfo \
    udisks2-dev \
    xcb-util-keysyms-dev \
&& rm -rf /var/cache/apk/*

WORKDIR /root

# Clone kdesrc-build
RUN git clone git://anongit.kde.org/kdesrc-build.git

ENV PATH="/root/kdesrc-build:$PATH"

# Provide base configuration
COPY kdesrc-buildrc /root

# Setup metadata
RUN kdesrc-build --metadata-only

LABEL version="3.7" \
  description="Sets up an Alpine Linux environment to use for building Qt5 and KF5 software with kdesrc-build" \
       author="Michael Pyne <mpyne@kde.org>"
