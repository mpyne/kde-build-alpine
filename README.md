This should be as easy as "docker build .".  I haven't given the resulting image or container any
meaningful names, but I encourage you to do so.  Note that the kdesrc-build is configured to delete
source *and* build dir after successful install, and to abort at the first failure.

It's worked for me recently, hopefully it will for you as well.

 - mpyne 7 Oct 2017
